
import UIKit
import CoreMotion

func HFDispatchAfterTime(time: Double, codeBlock: @escaping (() -> ()))
{
    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(time * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC))
    { () -> Void in
        codeBlock()
    }
}

func HFDispatchToMainQueue(codeBlock: @escaping (() -> ()))
{
    if Thread.isMainThread
    {
        codeBlock()
    }
    else
    {
        DispatchQueue.main.async(execute: { () -> Void in
            codeBlock()
        })
    }
}

func HFDispatchToBackgroundQueue(codeBlock: @escaping (() -> ()))
{
    DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
        codeBlock()
    }
}
