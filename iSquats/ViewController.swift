//
//  ViewController.swift
//  iSquats
//
//  Created by Polumna on 19.10.16.
//  Copyright © 2016 Ilya Rudentsov. All rights reserved.

//data=Optional(x -0.336716 y 0.904648 z -0.195328 @ 54690.602020)
//data=Optional(x -0.776093 y -0.648361 z 0.017838 @ 54719.668724)
//data=Optional(x -0.320450 y 0.931122 z -0.121094 @ 54741.230780)


import UIKit
import CoreMotion
import AudioToolbox
class ViewController: UIViewController {

    @IBOutlet weak var countLabel: UILabel!
    
    var motionManager = CMMotionManager()
    
    var currentAccelData: CMAccelerometerData!
    var stayAccelData: CMAccelerometerData!
    var sitAccelData: CMAccelerometerData!
    
    var currentGyroData: CMGyroData!
    var stayGyroData: CMGyroData!
    var sitGyroData: CMGyroData!
    
    var gyroCycleCompleate = false
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    var squatsCount = 0
        {
        didSet{
            countLabel.text = "\(squatsCount)"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let device = UIDevice.current
        device.isProximityMonitoringEnabled = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.proximityChanged), name: NSNotification.Name.UIDeviceProximityStateDidChange, object: device)

        //startMonitorAccelerate()
        //startMonitorGyroscope()
        
    }
    
//-notifications:
    func proximityChanged(_ notification: Notification) {
        
        let device = notification.object as! UIDevice
        if(device.proximityState){
            beginSquates()
        }else{
            endSquates()
        }
    }
    
//-actions: 
    func startMonitorAccelerate(){
        motionManager.accelerometerUpdateInterval = 0.2
        motionManager.startAccelerometerUpdates(to: OperationQueue.current!) { (data: CMAccelerometerData?, error: Error?) in
            
            self.currentAccelData = data!
            //print("data=\(data)")
            
            //сел
            if( self.stayAccelData != nil && self.sitGyroData != nil && self.differentStayAndCurrentForAccel(offcet: 0.55) ){
                
                self.sitAccelData = data!
            }
            //встал
            if( self.sitAccelData != nil && !self.differentStayAndCurrentForAccel(offcet: 0.4) && self.sitGyroData != nil && self.differentStayAndCurrentForGyroUp()){
                HFDispatchToBackgroundQueue {
                    
                    //Если сработал +1 при доставании из кармана
                    HFDispatchAfterTime(time: 1.25, codeBlock: {
                        if(!UIDevice.current.proximityState){
                            self.squatsCount -= 1
                            //self.errorAlert()
                            HFDispatchToBackgroundQueue {
                                AudioServicesPlaySystemSound(1053)
                            }
                        }
                    })
                    
                    AudioServicesPlaySystemSound(1054)
                }
                
                self.sitAccelData = nil
                self.sitGyroData = nil
                self.gyroCycleCompleate = false
                self.squatsCount += 1
            }
            
        }
    }
    
    func startMonitorGyroscope(){
        motionManager.gyroUpdateInterval = 0.2
        motionManager.startGyroUpdates(to: OperationQueue.current!) { (data: CMGyroData?, error: Error?) in
            self.currentGyroData = data
            print("data.x=\(data?.rotationRate.x)  data.y=\(data?.rotationRate.y)")
            
            //сел
            if( self.stayGyroData != nil && self.differentStayAndCurrentForGyroSit() ){
                
                self.sitGyroData = data!
                self.gyroCycleCompleate = false
            }
            
            //встал
            if( self.sitGyroData != nil && self.differentStayAndCurrentForGyroUp() ){
                self.gyroCycleCompleate = true
            }
            
        }

    }

    
    func beginSquates(){
        
        
        self.startMonitorAccelerate()
        self.startMonitorGyroscope()
        
        HFDispatchAfterTime(time: 3.0, codeBlock: {
            //Здесь сигнал
            if(UIDevice.current.proximityState){
                self.stayAccelData = self.currentAccelData
                self.sitAccelData = nil
                
                self.stayGyroData = self.currentGyroData
                self.sitGyroData = nil
                
                HFDispatchAfterTime(time: 0.2, codeBlock: {
                    AudioServicesPlaySystemSound(1054)
                })
            }
            
        })
    }
    
    func endSquates(){
        //HFDispatchToBackgroundQueue {
        //AudioServicesPlaySystemSound(1053)
        //}
        motionManager.stopGyroUpdates()
        motionManager.stopAccelerometerUpdates()
        self.stayAccelData = nil
        self.sitAccelData = nil
        
        self.stayGyroData = nil
        self.sitGyroData = nil
    }
    
//-error:
    
    func errorAlert(){
        let alert = UIAlertController(title: "Последнее приседание ошибочно", message: nil, preferredStyle: UIAlertControllerStyle.alert)
        self.present(alert, animated: true, completion: nil)
        HFDispatchAfterTime(time: 1.5) {
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
//-helpers:
    func differentStayAndCurrentForAccel(offcet: Double)->Bool{
        
        if(self.currentAccelData != nil && self.stayAccelData != nil){
            //let offcet = 0.4
            return ( (self.currentAccelData.acceleration.x > self.stayAccelData.acceleration.x + offcet || self.currentAccelData.acceleration.x < self.stayAccelData.acceleration.x - offcet) || (self.currentAccelData.acceleration.y > self.stayAccelData.acceleration.y + offcet || self.currentAccelData.acceleration.y < self.stayAccelData.acceleration.y - offcet) || (self.currentAccelData.acceleration.z > self.stayAccelData.acceleration.z + offcet || self.currentAccelData.acceleration.z < self.stayAccelData.acceleration.z - offcet) )
            
        }
        return false
    }
    
    func differentStayAndCurrentForGyroSit()->Bool{
        
        if(self.currentGyroData != nil){
            let offcet = -0.9000
            return (self.currentGyroData.rotationRate.z <= offcet || self.currentGyroData.rotationRate.x <= offcet || self.currentGyroData.rotationRate.y <= offcet)
            
        }
        return false
    }
    
    func differentStayAndCurrentForGyroUp()->Bool{
        
        if(self.currentGyroData != nil){
            let offcet = 0.9000
            return (self.currentGyroData.rotationRate.z >= offcet || self.currentGyroData.rotationRate.x >= offcet || self.currentGyroData.rotationRate.y >= offcet)
            
        }
        return false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
//-buttons actions:
    
    @IBAction func resetButtonTouch(_ sender: UIButton) {
        
        squatsCount = 0
    }


}


