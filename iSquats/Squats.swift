//
//  Squats.swift
//  iSquats
//
//  Created by Polumna on 20.10.16.
//  Copyright © 2016 Ilya Rudentsov. All rights reserved.
//

import Foundation
import CoreMotion
import AudioToolbox
import UIKit
class Squats {
    
    var motionManager = CMMotionManager()
    
    var currentAccelData: CMAccelerometerData!
    var stayAccelData: CMAccelerometerData!
    var sitAccelData: CMAccelerometerData!
    
    var currentGyroData: CMGyroData!
    var stayGyroData: CMGyroData!
    var sitGyroData: CMGyroData!
    
    var gyroCycleCompleate = false
    var countLabel = UILabel()
    
    var squatsCount = 0
        {
        didSet{
            countLabel.text = "\(squatsCount)"
        }
    }
    
    init(label: UILabel) {
        countLabel = label
    }
    
    func start(){
        startMonitorAccelerate()
        startMonitorGyroscope()
        
        HFDispatchAfterTime(time: 3.0, codeBlock: {
            
            //Здесь сигнал
            if(UIDevice.current.proximityState){
                HFDispatchToBackgroundQueue {
                    AudioServicesPlaySystemSound(1054)
                }
                self.stayAccelData = self.currentAccelData
                self.sitAccelData = nil
                
                self.stayGyroData = self.currentGyroData
                self.sitGyroData = nil
            }
            
        })

    }
    
    func stop(){
        motionManager.stopAccelerometerUpdates()
        motionManager.stopGyroUpdates()
        self.stayAccelData = nil
        self.sitAccelData = nil
        
        self.stayGyroData = nil
        self.sitGyroData = nil
        
        self.gyroCycleCompleate = false
    }
    
    func startMonitorAccelerate(){
        motionManager.accelerometerUpdateInterval = 0.25
        motionManager.startAccelerometerUpdates(to: OperationQueue.current!) { (data: CMAccelerometerData?, error: Error?) in
            
            self.currentAccelData = data!
            //print("data=\(data)")
            
            //сел
            if( self.stayAccelData != nil && self.sitGyroData != nil && self.differentStayAndCurrentForAccel() ){
                
                self.sitAccelData = data!
            }
            //встал
            if( self.sitAccelData != nil && !self.differentStayAndCurrentForAccel() && self.sitGyroData != nil && self.differentStayAndCurrentForGyroUp()){
                HFDispatchToBackgroundQueue {
                    AudioServicesPlaySystemSound(1054)
                }
                
                self.sitAccelData = nil
                self.sitGyroData = nil
                self.gyroCycleCompleate = false
                self.squatsCount += 1
                
                //self.stayAccelData = data
            }
            
        }
    }
    
    func startMonitorGyroscope(){
        motionManager.gyroUpdateInterval = 0.1
        motionManager.startGyroUpdates(to: OperationQueue.current!) { (data: CMGyroData?, error: Error?) in
            self.currentGyroData = data
            //print("data=\(data?.rotationRate.z)")
            
            //сел
            if( self.stayGyroData != nil && self.differentStayAndCurrentForGyroSit() ){
                
                self.sitGyroData = data!
                self.gyroCycleCompleate = false
            }
            
            //встал
            if( self.sitGyroData != nil && self.differentStayAndCurrentForGyroUp() ){
                self.gyroCycleCompleate = true
            }
            
        }
        
    }
    
//-helpers:
    func differentStayAndCurrentForAccel()->Bool{
        
        if(self.currentAccelData != nil && self.stayAccelData != nil){
            let offcet = 0.5
            return ( (self.currentAccelData.acceleration.x > self.stayAccelData.acceleration.x + offcet || self.currentAccelData.acceleration.x < self.stayAccelData.acceleration.x - offcet) || (self.currentAccelData.acceleration.y > self.stayAccelData.acceleration.y + offcet || self.currentAccelData.acceleration.y < self.stayAccelData.acceleration.y - offcet) || (self.currentAccelData.acceleration.z > self.stayAccelData.acceleration.z + offcet || self.currentAccelData.acceleration.z < self.stayAccelData.acceleration.z - offcet) )
            
        }
        return false
    }
    
    func differentStayAndCurrentForGyroSit()->Bool{
        
        if(self.currentGyroData != nil){
            let offcet = -2.5000
            return (self.currentGyroData.rotationRate.z <= offcet || self.currentGyroData.rotationRate.x <= offcet || self.currentGyroData.rotationRate.y <= offcet)
            
        }
        return false
    }
    
    func differentStayAndCurrentForGyroUp()->Bool{
        
        if(self.currentGyroData != nil){
            let offcet = 1.5000
            return (self.currentGyroData.rotationRate.z >= offcet || self.currentGyroData.rotationRate.x >= offcet || self.currentGyroData.rotationRate.y >= offcet)
            
        }
        return false
    }
    
}
