//
//  HowToViewController.swift
//  iSquats
//
//  Created by Polumna on 19.10.16.
//  Copyright © 2016 Ilya Rudentsov. All rights reserved.
//

import Foundation
import  UIKit

class HowToViewController: UIViewController{
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        
    }
    
    @IBAction func backButtonTouch(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
